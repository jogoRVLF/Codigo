extends TextureRect

onready var option = get_node("OptionButton")

func _ready():

	print("cena01 inicializada")
	
	option.add_item("Escolha uma Opção")
	option.add_separator()
	option.add_item("OPÇÃO A) Mais da metade")
	option.add_item("OPÇÃO B) Menos da metade")
	option.add_item("OPÇÃO C) Nenhuma")

func _on_btn_menu_pressed():
	#Quando menu é precionado retorna para a cena 'menu' retorna ao menu
	print("Volta para o menu.")
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_pressed():
	print("Mudar cena.")
	get_tree().change_scene("res://scenes/perguntas/cena01 - transição.tscn")
