extends TextureRect

#option é um nó OptonButton 
#export (NodePath) var option_path
#onready var option = get_node(option_path)
onready var option = get_node("option")

var selected
var dados

func _ready():
	var f = File.new()
	
	print("cena05 inicializada")
	
	f.open("data", File.READ)
	dados = f.get_var()
	selected = dados["Partido"]

	option.add_item("Escolha uma Opção")
	option.add_separator()
	#option recebe as opções de itens
	if selected == "Jacobino":
		option.add_item("OPÇÃO A) O Sr. Robespierre é nada mais do que um representante do povo")
		option.add_item("OPÇÃO B) Não nos parece clara a culpa do Sr. Robespierre pelos massacres de setembro")
	elif selected == "Girondino":
		option.add_item("OPÇÃO A) O Sr. Robespierre é um insurgente")
		option.add_item("OPÇÃO B) Fica clara a influência do Sr. Maximilien Robespierre nos eventos de setembro")
	elif selected == "Planície":
		option.add_item("OPÇÃO A) Não é possível afirmar a culpabilidade do Sr. Robespierre")
		option.add_item("OPÇÃO B) É apenas permitido considerar os efeitos dos eventos de setembro")
	
	f.close()
	
func _on_btn_menu_pressed():
	#Quando menu é precionado retorna para a cena 'menu' retorna ao menu
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_pressed():
	#Quando next é pressionado 
	#name recebe o nome digitado pelo usuario
	#dados recebe name, a opção de partido selecionada 
	#e o status do andamento do jogo
	#avança para a próxima cena
	var name = dados["Nome"]
	var f = File.new()

	f.open("data", File.READ)
	dados = f.get_var()
	f.close()
	f.open("data", File.WRITE)
	dados = {"Nome":name, "Partido":selected, "status":"cena06"}
	f.store_var(dados)
	f.close()
	get_tree().change_scene("res://scenes/perguntas/cena06.tscn")
