extends TextureRect

#option é um nó OptonButton 
#export (NodePath) var option_path
#onready var option = get_node(option_path)
onready var option = get_node("option")

var selected
var dados

func _ready():
	var f = File.new()
	
	print("cena03 inicializada")
	
	option.add_item("Escolha uma Opção")
	option.add_separator()
	
	f.open("data", File.READ)
	dados = f.get_var()
	selected = dados["Partido"]
	
	#option recebe as opções de itens
	if selected == "Jacobino":
		option.add_item("OPÇÃO A) Deve-se declarar a República e esta deve ser do povo")
		option.add_item("OPÇÃO B) A República deve ser proclamada em união com uma extrema democracia")
	elif selected == "Girondino":
		option.add_item("OPÇÃO A) A possibilidade mais coerente nos atuais rumos é declarar a República")
		option.add_item("OPÇÃO B) Mesmo sendo difícil a sua consolidação, deve-se estabelecer a República")
	elif selected == "Planície":
		option.add_item("OPÇÃO A) Considera-se mais justo a proclamação da República")
		option.add_item("OPÇÃO B) Tudo se encaminha para que a República seja o melhor modelo político")
	
	f.close()

func _on_btn_menu_pressed():
	#Quando menu é precionado retorna para a cena 'menu' retorna ao menu
	get_tree().change_scene("res://scenes/menu.tscn")

func _on_next_pressed():
	#Quando next é pressionado 
	#name recebe o nome digitado pelo usuario
	#dados recebe name, a opção de partido selecionada 
	#e o status do andamento do jogo
	#avança para a próxima cena
	var f = File.new()
	var name = dados["Nome"]
	f.open("data", File.WRITE)
	dados = {"Nome":name, "Partido":selected, "status":"cena04"}
	f.store_var(dados)
	f.close()
	get_tree().change_scene("res://scenes/perguntas/cena04.tscn")
