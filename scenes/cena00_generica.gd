extends TextureRect

func _ready():
	print("cena00 inicializada")
	var f = File.new()
	f.open("data", File.READ)
	var dados = f.get_var()
	$sound.volume_db = dados["sound"]
	f.close()
	
func _on_btn_menu_pressed():
	#Quando menu é precionado retorna para a cena 'menu'
	get_tree().change_scene("res://scenes/menu.tscn")
	
func _on_next_pressed():
	#Quando next é precionado inicia um arquivo
	#carrega e atualiza os dados do arquivo
	#avança para a próxima cena
	var f = File.new()
	f.open("data", File.READ)
	var dados = f.get_var()
	$sound.volume_db = dados["sound"]
	f.close()
	f.open("data", File.WRITE)
	#dados = {"Nome":name, "Partido":selected, "status":"cena00_generica", "sound":dados["sound"]}
	f.store_var(dados)
	f.close()
	#get_tree().change_scene("res://scenes/....tscn")